# Toolbox Project Overview

## Introduction

This repository contains some cmd.exe and Windows tools that can be used in scripts (batch and PowerShell files) to achieve otherwise impossible or difficult things. The tools are extensions to scripts that I had to write and maintain as Windows/CMD/DOS batch files. They should make my job easier and they should still work now. For now I just changed the compiler from Visual Studio to the GNU C++ compiler (MinGW) and got the code working again. In the meantime, new tools are being added again.

The Toolbox is a project which provides mostly simple commands for the command-line interface/shell. When writing batch files one often needs certain tools to simplify the work. For instance, if you need to parse a date to separate its fields (day, month, year) you have to ensure to use the right language because the output of date is localized and changes on different systems. Here comes my date command. Or if you need to pause a batch script a lot of people abuse the ping command and even this allows only a precision of about one second.

Unfortunately there are only very few websites providing working and documented binaries to download. Most of them are not fully trustable and so I wrote some small tools which I provide with full source code. So you can always compile them yourself if you have to ensure (e.g. in your company, or in own projects for a customer) that no malicious software will be installed.

## History

Most of the tools were created and used between 08/01/2003 and 05/07/2007. Further development stopped abruptly when the provider of the developer platform, BerliOS, ceased its services for the open source software community. The tools come from a backup of my former BerliOS repository and are now to be gradually modernized.

Many of the things that were necessary back then are of no interest today. For example, the sync command was used to be able to switch off PCs "hard" afterwards and thus save the time to shut down if necessary. Other tools are still useful, but could be replaced by PowerShell commands as well. Then even new commands are added. For example, it is still not possible using cmdkey.exe under Windows to read stored credentials again and continue using them in a script. The credtool command, for example, can be used for this.

## List of tools

| Tool | Description |
| ---- | ----------- |
| [addtoclipboard](src/addtoclipboard/README.md) | Adds text to already existing text in the clipboard. |
| [cliptool](src/cliptool/README.md) | This tool clears the data currently stored in the clipboard. |
| [copy-backup](src/copy-backup/README.md) | Copies a locked file using backup mode. |
| [copy-vss](src/copy-vss/README.md) | Copies a locked file using the VSS. |
| [credtool](src/credtool/README.md) | Returns a previously stored, named credential of the current user from the Windows credential manager. It is useful in case you need to use a username and password combination in a script, but you don't want to store the data in clear text there. |
| [date](src/date/README.md) | This tool returns the current date in a language-independent standard format that can be parsed from a CMD batch script without the need for adaptations to language- or operating system-dependent output formats. |
| [getfolderpath](src/getfolderpath/README.md) | Returns the path which belongs to a Windows folder name. |
| [isadmin](src/isadmin/README.md) | The isadmin tool checks if it runs as Administrator and returns the result as error code. |
| [keypressed](src/keypressed/README.md) | Checks if a certain key combination is pressed. Allows a script to react. |
| [say](src/say/README.md) | Outputs a text in spoken form. |
| [shutdown](src/shutdown/README.md) | Shuts down or reboots the computer. |
| [sleep](src/sleep/README.md) | A simple sleep command which gets the time in milliseconds and waits this time. |
| [sync](src/sync/README.md) | Syncs all drives. |
| [time](src/time/README.md) | This tool returns the current time in a language-independent standard format that can be parsed from a CMD batch script without the need for adaptations to language- or operating system-dependent output formats. |
| [volume](src/volume/README.md) | The volume command line tool sets the volume and the mute state of a PC. |
| [wlan-info](src/wlan-info/README.md) | This tool displays data about the WLANs it finds. |

## License

The binary files and of course all source code may be used under the terms of the [MIT License](LICENSE). I hope you find one or the other tool useful. Thank you for using this software.

Please open a [ticket](https://gitlab.com/Munahid/toolbox/-/issues) if you have a good idea for possible improvements or not yet existing but much needed small applications.
