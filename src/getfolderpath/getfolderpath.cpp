// The getfolderpath tool returns the path which belongs to a Windows folder name.

#ifndef _UNICODE
#define _UNICODE
#endif
#ifndef UNICODE
#define UNICODE
#endif

#define WIN32_LEAN_AND_MEAN		       // Exclude rarely-used stuff from Windows headers.

#include <windows.h>
#include <shellapi.h>
#include <locale.h>
#include <shlobj.h>
#include <shlwapi.h>

#include <iostream>
#include <string>

#include "../common/CommandLineArguments.h"

int main()
{
  CommandLineArguments args;

  setlocale(LC_ALL, "");

  args.addParameter("", "", "CSIDL number (see docs).",
                    CommandLineArguments::DataType::Integer,
                    CommandLineArguments::ParaType::Required);

  args.addInfo(L"Returns the path which belongs to a Windows standard folder name.");
  args.addHelp(L"<CSIDL number>");
  args.check();

  if (args.count() != 2) {
    std::wcout << args.getW(0) << L": <CSIDL number>" << std::endl;
    std::wcout << L"Please read the doc for a list of valid CSIDL numbers." << std::endl;
    return 2;
  }

  int csidl = std::stoi(args.getW(1));

  // https://docs.microsoft.com/en-us/windows/win32/api/shlobj_core/nf-shlobj_core-shgetfolderpathw
  // CSIDL_FLAG_CREATE
  TCHAR path[MAX_PATH];
  HRESULT rc = ::SHGetFolderPath(NULL, csidl, NULL, 0, path);

  if (SUCCEEDED(rc)) {
    wprintf_s(TEXT("%s\n"), path);
  }

  return 0;
}
