# GetFolderPath command

CSIDL List:
http://msdn2.microsoft.com/en-us/library/ms649274.aspx

http://msdn2.microsoft.com/en-us/library/aa969409.aspx
http://msdn2.microsoft.com/en-us/library/aa969388.aspx
http://msdn2.microsoft.com/en-us/library/ms647764.aspx !!

Lets say you want to get this path to install something into it:

  C:\Dokumente und Einstellungen\All Users\Startmenü

How can one retrieve it? The environment variable "%ALLUSERSPROFILE%" returns only this part:

  C:\Dokumente und Einstellungen\All Users

Unfortunately the folder "Startmenü" is localized, for example english Windows installations use "Startmenu". Microsoft uses CSIDLs to handle special folders. Use the GetFolderPath program and provide the CSIDL (as decimal value) of the folder which name you want to retrieve.

The above path with your locale is "GetFolderPath.exe 11"

- No user accounts possible (until now).
- Old windows versions have to link against an additional DLL from Microsoft. It may be supplied with your program.
- Folder creation isn't implemented. Ask me if you need it.
