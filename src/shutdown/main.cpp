// This shuts the PC down.

#ifndef _UNICODE
#define _UNICODE
#endif
#ifndef UNICODE
#define UNICODE
#endif

#define WIN32_LEAN_AND_MEAN		                           // Exclude rarely-used stuff from Windows headers.

#include <windows.h>
#include <tchar.h>
#include <iostream>

int main(int argc, char *argv[])
{
  HANDLE hToken;                                           // Berechtigung zum Herunterfahren anfordern.
  TOKEN_PRIVILEGES tkp;

  UINT shutdownFlags   = EWX_SHUTDOWN | EWX_FORCE;         // Standardmäßig wird der Computer heruntergefahren.
  DWORD shutdownReason = SHTDN_REASON_MAJOR_OTHER | SHTDN_REASON_MINOR_OTHER;

  if (argc > 1) {                                          // Parameter auswerten.
    std::string param = argv[1];
    if (param == "reboot") {
      shutdownFlags = EWX_REBOOT | EWX_FORCE;
    } else if (param != "shutdown") {
      std::wcerr << L"Ungültiger Parameter. Verwenden Sie 'shutdown' oder 'reboot'." << std::endl;
      return 1;
    }
  }

  if (!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken)) {
    std::wcerr << L"Fehler: Prozess-Token konnte nicht geöffnet werden." << std::endl;
    return 1;
  }

  if (!LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME, &tkp.Privileges[0].Luid)) {
    std::wcerr << L"Fehler: Berechtigung zum Herunterfahren konnte nicht abgerufen werden." << std::endl;
    CloseHandle(hToken);
    return 2;
  }

  tkp.PrivilegeCount = 1;
  tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

  if (!AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES)NULL, 0)) {
    std::wcerr << L"Fehler: Berechtigungen konnten nicht angepasst werden." << std::endl;
    CloseHandle(hToken);
    return 3;
  }

  // Windows erfordert die Berechtigung SE_SHUTDOWN_NAME, um das System herunterzufahren.
  // Diese wird über AdjustTokenPrivileges gesetzt.Das Programm muss mit Administratorrechten
  // ausgeführt werden, um die Berechtigung zum Herunterfahren anzupassen.

  if (GetLastError() == ERROR_NOT_ALL_ASSIGNED) {
    std::wcerr << L"Fehler: Berechtigung wurde nicht gewährt." << std::endl;
    CloseHandle(hToken);
    return 4;
  }

  // System herunterfahren. Anwendungen schließen erzwingen (auch nicht antwortende Prozesse), Grund: Other.
  // or EWX_REBOOT

  if (!ExitWindowsEx(shutdownFlags, shutdownReason)) {
    std::wcerr << L"Fehler: Herunterfahren/Neustart fehlgeschlagen." << std::endl;
    CloseHandle(hToken);
    return 5;
  }

  CloseHandle(hToken);

  return 0;
}
