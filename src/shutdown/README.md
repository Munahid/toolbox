# shutdown command

The Shutdown command lets you reboot or shutdown your PC with one
single click. There are no dialogs or GUI interactions needed.
