// Handles command line arguments.

#ifndef _UNICODE
#define _UNICODE
#endif
#ifndef UNICODE
#define UNICODE
#endif

#define WIN32_LEAN_AND_MEAN		       // Exclude rarely-used stuff from Windows headers.

#include <windows.h>
#include <shellapi.h>                  // CommandLineToArgvW()

#include <iostream>
#include <string>
#include <codecvt>                     // deprecated
#include <locale>
#include <type_traits>                 // static_assert()
#include <vector>
//#include <stdexcept>

#pragma once

// Helper function for UTF-16 to UTF-8 conversion

std::string utf16_to_utf8(const std::wstring& wstr)
{
  if (wstr.empty()) return {};

  // Calculate the required size for the UTF-8 string
  int size_needed = WideCharToMultiByte(CP_UTF8, 0, wstr.c_str(), -1, nullptr, 0, nullptr, nullptr);
  if (size_needed <= 0) {
    //throw std::runtime_error("WideCharToMultiByte failed during conversion.");
    return "";
  }

  std::string str_to(size_needed - 1, '\0'); // Exclude null terminator
  WideCharToMultiByte(CP_UTF8, 0, wstr.c_str(), -1, &str_to[0], size_needed, nullptr, nullptr);
  return str_to;
}

class CommandLineArguments
{
  public:
    CommandLineArguments();
   ~CommandLineArguments();

    CommandLineArguments & operator=(const CommandLineArguments &) = delete;
    CommandLineArguments(const CommandLineArguments &) = delete;

    enum class DataType {
      String,                          // String parameter. Wide.
      Integer,                         // Integer parameter.
      Boolean,                         // Boolean parameter with a required value 'true' or 'false' or 1/0.
      Flag                             // Boolean parameter without a separate value. Default 'false'.
    };

    enum class ParaType {
      Required = true, Optional = false
    };

    enum class MultipleType {
      Multiple = true, Single = false
    };

    struct Arg {
      std::wstring value       = L"";
      std::string  shortName   = "";
      std::string  longName    = "";
      std::string  description = "";
      DataType     type        = DataType::String;
      ParaType     required    = ParaType::Optional;
      MultipleType multiple    = MultipleType::Single;    // May occur multiple times. TODO.
    };

    struct Group {
      std::string  groupName   = "";
      std::string  members     = "";
      bool         required    = false;
    };

    void addParameter(const std::string &shortName, const std::string &longName, const std::string &description, const DataType type = DataType::String, const ParaType required = ParaType::Optional, const MultipleType multiple = MultipleType::Single);

    void addGroup(const std::string &groupName, const std::string &members, const bool required = false);

    void addHelp(const std::wstring &helpText) { m_helpText = helpText; };
    void addInfo(const std::wstring &infoText) { m_infoText = infoText; };

    void showHelp() const
    {
      std::wcout << m_helpText << std::endl;
    }

    void showInfo() const
    {
      std::wcout << m_infoText << std::endl;
    }

    int count() const { return m_argCount; };

    std::wstring getNamedW(const std::string &name);
    std::wstring getW(const int i);
    std::string get(const int i);

    bool exists(const std::string &name) const;

    bool check() const;

  protected:
    LPWSTR       *m_argList;
    int           m_argCount;
    std::wstring  m_helpText;
    std::wstring  m_infoText;

    std::vector<Arg>   m_args   = {};
    std::vector<Group> m_groups = {};
};

CommandLineArguments::CommandLineArguments()
 : m_argList(nullptr), m_argCount(0), m_helpText(L""), m_infoText(L"")
{
  m_argList = ::CommandLineToArgvW(GetCommandLineW(), &m_argCount);
  if (NULL == m_argList) {
    wprintf(L"Error (CommandLineToArgvW): Conversion failed.\n");
    return;
  }
}

CommandLineArguments::~CommandLineArguments()
{
  if (m_argList) {
    LocalFree(m_argList);              // Free memory allocated for arguments.
    m_argList = nullptr;
  }
}

void CommandLineArguments::addParameter(const std::string &shortName, const std::string &longName, const std::string &description, const DataType type /*= DataType::String*/, const ParaType required /*= ParaType::Optional*/, const MultipleType multiple /*= MultipleType::Single*/)
{
  Arg arg;

  arg.shortName   = shortName;
  arg.longName    = longName;
  arg.description = description;
  arg.type        = type;
  arg.required    = required;
  arg.multiple    = multiple;

  m_args.push_back(arg);
};

void CommandLineArguments::addGroup(const std::string &groupName, const std::string &members, const bool required /*= false*/)
{
  Group group;

  group.groupName = groupName;
  group.members   = members;
  group.required  = required;

  m_groups.push_back(group);
};

// Get a parsed and named argument.

std::wstring CommandLineArguments::getNamedW(const std::string &name)
{
  if (m_argList == nullptr)
    return L"";

  // TODO.

  return L"";
}

std::wstring CommandLineArguments::getW(const int i)
{
  if ((m_argList == nullptr) || (i < 0))
    return L"";

  return m_argList[i];
}

std::string CommandLineArguments::get(const int i)
{
  if ((m_argList == nullptr) || (i < 0))
    return "";

  std::wstring ws = getW(i);
  if (ws.length() == 0)
    return "";

  return utf16_to_utf8(ws);
  //std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> conv;
  //return conv.to_bytes(ws);

  //using convert_typeX = std::codecvt_utf8<wchar_t>;
  //std::wstring_convert<convert_typeX, wchar_t> converterX;
  //return converterX.from_bytes(str);
}

#if 0
  if (m_argCount != 3) {
    std::wcout << m_argList[0] << ": <command> <name of credential>" << std::endl;
    std::wcout << "Commands: -u (username) or -p (password) or -c (comment)" << std::endl;
    return 2;
  }

  if (!(command == L"-c") && !(command == L"-p") && !(command == L"-u")) {
    std::wcout << "Unknown command." << std::endl;
	return 3;
  }
#endif

// If the named argument is a Flag, it returns true if the
// argument was given without a value. If it is a Boolean,
// it returns the boolean value of the argument. If the
// argument's value was missed, it returns false.

bool CommandLineArguments::exists(const std::string &name) const
{

  return false;
}

// Parses all arguments and returns false if there is an error.

bool CommandLineArguments::check() const
{
  // Go through all the parameter strings and map them to the supported parameters.
  // Short and long arguments may start with nothing (if there are no other
  // unnamed/text arguments), "/", "-", or "--".
  // Special cases: --argument=value or --argument="value 123"

//  bool searchArgument = true;
//  std::string name;
  for (int i=0; i<m_argCount; i++) {
    std::wstring arg = m_argList[i];
    std::wcout << "ARG: " << i << " (" << arg << ")" << std::endl;

/* TODO
    if (searchArgument) {
      for (size_t j = 0; j < m_args.size(); j++) {
        name = m_args[j].shortName;
        if (name.length() > 0) {
          if (arg.starts_with(name)) {  // if (arg.substr(0, name.size()) == name) {

          }
        }
      }
    }

    // std::stoi(arg.substr(prefix.size()))
    //for (size_t i = 0; i < v.size(); i++) {
    //    std::cout << v[i] << std::endl;
    //}
*/    
  }



  // Check for unknown parameters.

  

  // Check for required parameters.

  

  // Check argument counts.



  return true; // No errors.
}
