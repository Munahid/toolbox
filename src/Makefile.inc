# Makefile for a tool.

APPNAME = $(notdir $(CURDIR))
APPTYPE ?= Console

CC := g++
CFLAGS := -Werror -Wall -Wextra -Wshadow -Weffc++ -std=c++26 -pedantic -Wstrict-overflow=5 -Wno-unused-parameter -g -O2
LFLAGS := -static -static-libstdc++ -static-libgcc

ifeq ($(APPTYPE),Console)
LINKADD := -Wl,-subsystem,console -mconsole
endif
ifeq ($(APPTYPE),DLL)
LINKADD := -Wl,-subsystem,windows -mwindows -lkernel32 -luser32
endif
ifeq ($(APPTYPE),Windows)
LINKADD := -Wl,-subsystem,windows -mwindows -lkernel32 -luser32
endif

LFLAGS := $(LFLAGS) $(LINKADD)

SRCS := $(wildcard *.cpp)
OBJS := $(patsubst %.cpp,%.o,$(SRCS))
DEPS := $(SRCS:.cpp=.d)

TARGET = ../../bin/$(APPNAME).exe

all: build test deploy

build: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(LFLAGS) $^ -o $@ $(MORELFLAGS)
	@strip $(TARGET)

%.o: %.cpp
	$(CC) $(CFLAGS) $(MORECFLAGS) -MMD -MP -c $<

clean:
	rm -f *.o *.d

cleanall: clean
	rm -f $(TARGET)

.PHONY: all clean cleanall

-include $(DEPS)

test:

deploy:
