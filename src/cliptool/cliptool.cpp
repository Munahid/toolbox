// This tool clears the data currently stored in the clipboard.

#ifndef _UNICODE
#define _UNICODE
#endif
#ifndef UNICODE
#define UNICODE
#endif

#define WIN32_LEAN_AND_MEAN		       // Exclude rarely-used stuff from Windows headers.

#include <windows.h>

int main(int argć, char *argv[])
{
  if (OpenClipboard(NULL)) {
    EmptyClipboard();
    CloseClipboard();
  }
}
