# cliptool command

This tool clears the data currently stored in the clipboard.

In my earlier batch scripts I used `%windir%\System32\cmd.exe /c "echo off | clip"` to clear the clipboard. I even had it as a shortcut on the desktop. Only sometimes - I don't know why - it didn't work and the content was retained anyway. It just failed in some cases. The Windows clipboard settings window thought there was still content in it. However, since it is often important that the content disappears completely, individual dropouts are a problem. The program solves it.
