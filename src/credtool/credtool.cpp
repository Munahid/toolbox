// This tool returns the stored named credential from the
// Windows credential manager.

#ifndef _UNICODE
#define _UNICODE
#endif
#ifndef UNICODE
#define UNICODE
#endif

#define WIN32_LEAN_AND_MEAN		       // Exclude rarely-used stuff from Windows headers.

#include <windows.h>
#include <wincred.h>

#include <iostream>
#include <string>
#include <codecvt>
#include <locale>
#include <type_traits>

#include "../common/CommandLineArguments.h"

int main()
{
  CommandLineArguments args;

  args.addParameter("", "", "Name of the credential.",
                    CommandLineArguments::DataType::String,
                    CommandLineArguments::ParaType::Required);
  args.addParameter("u", "username", "Retrieve the username field.",
                    CommandLineArguments::DataType::Flag,
                    CommandLineArguments::ParaType::Optional);
  args.addParameter("p", "password", "Retrieve the password field.",
                    CommandLineArguments::DataType::Flag,
                    CommandLineArguments::ParaType::Optional);
  args.addParameter("c", "comment", "Retrieve the comment field.",
                    CommandLineArguments::DataType::Flag,
                    CommandLineArguments::ParaType::Optional);
  args.addGroup("command", "u p c", true);

  args.addInfo(L"Retrieves fields from the credentials database.");
  args.addHelp(L"<command> <name of credential>");
  args.check();

  if (args.count() != 3) {
    std::wcout << args.getW(0) << ": <command> <name of credential>" << std::endl;
    std::wcout << "Commands: -u (username) or -p (password) or -c (comment)" << std::endl;
    return 2;
  }

  std::wstring command = { args.getW(1) };
  std::wstring name    = { args.getW(2) };

  if (!(command == L"-c") && !(command == L"-p") && !(command == L"-u")) {
    std::wcout << "Unknown command." << std::endl;
	return 3;
  }

  //wchar_t target[100] = L"";
  //std::wstring_convert< std::codecvt<wchar_t,char,std::mbstate_t> > converter;
  //std::wstring ws = converter.from_bytes(argv[1]);

  PCREDENTIALW cred = nullptr;

  BOOL ok = CredRead(name.c_str(), CRED_TYPE_GENERIC, 0, &cred);
  if (! ok) {
    // or with a standard message:
    // https://docs.microsoft.com/en-us/windows/win32/debug/retrieving-the-last-error-code
    DWORD errorCode = GetLastError();

    std::wcerr << "Error (CredRead): " << errorCode << std::endl;

    switch (errorCode) {
      case ERROR_NOT_FOUND:
        std::wcerr << "No credential exists with the specified target name." << std::endl;
        break;
      case ERROR_NO_SUCH_LOGON_SESSION:
        std::wcerr << "The logon session does not exist or there is no credential set associated with this logon session. Network logon sessions do not have an associated credential set." << std::endl;
        break;
      case ERROR_INVALID_FLAGS:
        std::wcerr << "A flag that is not valid was specified for the Flags parameter." << std::endl;
        break;
      case 87:
        std::wcerr << "Credential name was empty." << std::endl;
        break;
      default:
        std::wcerr << "Unknown error." << std::endl;
    }
    return 4;
  }

  if (command == L"-c")
    std::wcout << (wchar_t const *)cred->Comment << std::endl;
  if (command == L"-p")
    std::wcout << (wchar_t const *)cred->CredentialBlob << std::endl;
  if (command == L"-u")
    std::wcout << (wchar_t const *)cred->UserName << std::endl;

  if (cred)
    CredFree(cred);
}
