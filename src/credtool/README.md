# credtool command

Returns a stored, named credential of the current user from the Windows credential manager. Use "credread.exe -u PasswordName" to get the corresponding username and "credread.exe -p PasswordName" to retrieve the credential itself. A password may be stored with the command "cmdkey /generic:PasswordName /user:username /pass:password" or using the GUI with the Windows Credential Manager (in german this is here: Systemsteuerung\Benutzerkonten\Anmeldeinformationsverwaltung).

In a highly automated user environment, this command can be used to store passwords in the Windows Credential Manager and then use them in scripts or in the automated operation of websites. It is necessary to avoid storing passwords unencrypted. So if you have scripts that contain passwords, you can at least use this to keep the password out of the script and the repository. It is not really completely secure either. There are similar solutions with PowerShell with extension packages, but none that are included by default.

You often see scripts that contain passwords in plain text, for example for logging into a network share. Or there are scripts that hide the password from users using base64 encoding. This is now a little more secure:

Old:
```
# Uses a base64 encoded password. Generate with: echo "123" | base64 -
USERNAME='username'
PASSWORD=`echo "WW91IGFyZSByZWFsbHkgY3VyaW91cyA6LSkK" | base64 -d`

# Send form and grab the resulting web page for an result retrieval.
PAGE=`wget -q -O- -t 1 --no-cache \
        --post-data="SyncId=${SYNCID}&CmndTaste=${NOVATIME_CMD}&Ident1=${USERNAME}&Ident2=${PASSWORD}" \
        ${URL}`
```

New:

```
USERNAME=`credread.exe -u CoolWebsiteName`
PASSWORD=`credread.exe -p CoolWebsiteName`
...
```
