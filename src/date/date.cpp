// This tool returns the current date.

#ifndef _UNICODE
#define _UNICODE
#endif
#ifndef UNICODE
#define UNICODE
#endif

#define WIN32_LEAN_AND_MEAN		       // Exclude rarely-used stuff from Windows headers.

#include <windows.h>

#include <iostream>

int main(int argc, char *argv[])
{
  SYSTEMTIME systemtime;

  GetSystemTime(&systemtime);          // GetLocalTime()

  wchar_t buffer[] = L"yyyy-mm-dd\0";

  if (GetDateFormatEx(LOCALE_NAME_SYSTEM_DEFAULT, 0,
                      &systemtime, L"yyyy-MM-dd",
                      buffer, sizeof(buffer), NULL) == 0) {
    std::wcout << "Error." << std::endl;
    return 1;
  }

  std::wcout << buffer << std::endl;
}
