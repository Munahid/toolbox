# sleep command

This is a small command line application which simply sleeps. This allows
using a real sleep command in the CMD/DOS box. It works using microseconds,
also redirection isn't a problem. It is needed to stop the processing of
a sequence of commands while providing millisecond precision.

Why?

Since Windows 2000 there exists a timeout command, but this allows a
key-press to abort the wait. The "/T" can be omitted:
timeout /T 5
timeout 5
timeout /T 5 /NOBREAK
timeout /T 5 /NOBREAK >NUL
With /NOBREAK aborting by key-press isn't allowed (except an explicit CTRL-C) and with the redirect
it's possible to suppress the message `Waiting for ? seconds, press a key to continue ...`
But there are some caveats:

* Timeout actually only counts second multiples, therefore the wait time for /T 3 is actually something from 2 to 3 seconds. This is particularly annoying and disturbing for short wait times.
* You cannot use timeout in a block of code whose input is redirected, because it aborts immediately, throwing the error message ERROR: Input redirection is not supported, exiting the process immediately.. Hence timeout /T 5 < nul fails.

Others use the ping command:

You need a network, but the IP address 127.0.0.1/locslhost always exists. The standard ping interval is 1 second; so you need to do one more ping attempt than you want intervals to elapse:
ping 127.0.0.1 -n 4 > nul
This is the only reliable way to guarantee the minimum wait time and also redirection is no problem.
@ping -n 4 localhost> nul

One may use the choice command too:
choice /D J /C JN /N /T 3 >NUL

It should be noted that while the choice command is waiting 5 seconds for input, the timer gets reset each and every time the user presses a key. So it is possible that your batch file will never exit this choice step. Therefore it is also impossible to guarantee that the routine will only wait 5 seconds -- it could be longer because of a key press.
