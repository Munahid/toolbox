// This tool sleeps some time.

#ifndef _UNICODE
#define _UNICODE
#endif
#ifndef UNICODE
#define UNICODE
#endif

#define WIN32_LEAN_AND_MEAN		       // Exclude rarely-used stuff from Windows headers.

#include <windows.h>

#include <iostream>

int main(int argc, char *argv[])
{
  if (argc <= 1) {
    std::cout << "Usage: " << argv[0] << " <time to wait in ms>" << std::endl;
    return 1;
  }

  ::Sleep(std::atoi(argv[1]));
}
