// This tool displays a messagebox.

#ifndef _UNICODE
#define _UNICODE
#endif
#ifndef UNICODE
#define UNICODE
#endif

#define WIN32_LEAN_AND_MEAN		                           // Exclude rarely-used stuff from Windows headers.

#include <windows.h>
#include <shellapi.h>                                      // CommandLineToArgvW.
#include <string>
#include <sstream>

int main(int argcx, char *argvx[])
{
  LPCWSTR cmdLine = GetCommandLineW();                     // Kommandozeile abrufen.
  int argc = 0;
  LPWSTR* argv = CommandLineToArgvW(cmdLine, &argc);

  // Standardwerte für MessageBox.

  UINT boxType = MB_OK;                                    // Nur OK-Button.
  std::wstring title = L"Information";
  std::wstring message;

  if (argc <= 1) {                                         // Überprüfen, ob ein Parameter übergeben wurde
    return 1;
  }

  std::wstring param = argv[1];
  if (param == L"-info") {
    boxType |= MB_ICONINFORMATION;                         // Info-Icon.
    title = L"Info";
  } else if (param == L"-warn") {
    boxType = MB_OK | MB_ICONWARNING;                      // Warn-Icon.
    title = L"Warnung";
  } else if (param == L"-error") {
    boxType = MB_OK | MB_ICONERROR;                        // Fehler-Icon.
    title = L"Fehler";
  } else {
    message = L"Ungültiger Parameter.\nVerwenden Sie -info, -warn oder -error.";
    title = L"Fehler";
    boxType = MB_OK | MB_ICONERROR;                        // Fehler-Icon.
  }

  std::wstringstream messageStream;                        // Den Nachrichtentext sammeln.
  for (int i = 2; i < argc; ++i) {
    if (i > 2) {
      messageStream << L" ";                               // Add Leerzeichen zwischen Argumenten
    }
    messageStream << argv[i];
  }

  message = messageStream.str();

  MessageBoxW(nullptr, message.c_str(), title.c_str(), boxType);   // Show MessageBox.

  if (argv) {                                              // Free memory.
    LocalFree(argv);
  }

  return 0;
}
