//

#ifndef UNICODE
#define UNICODE
#endif 
#ifndef _UNICODE
#define _UNICODE
#endif 

#define WIN32_LEAN_AND_MEAN     // Exclude rarely-used stuff from Windows headers

#include <windows.h>
#include <strsafe.h>
#include <shellapi.h>

#include <iostream>

const DWORD bit29 = 0x20000000;

BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam)
{
  //int length = GetWindowTextLength(hwnd);

  wchar_t buffer[128];                                     // Unicode-Buffer
  int written = GetWindowTextW(hwnd, buffer, 128);
  if (written > 0 && wcsstr(buffer, (wchar_t*)lParam) != nullptr) {
    *(HWND*)lParam = hwnd;
    SetLastError(bit29 | 1);
    return FALSE;                                          // Found the target window, stop enumeration.
  }

  std::wcout << L"Win: " << buffer << std::endl;

  return TRUE;                                             // Continue enumeration.
}

int main()
{
  LPWSTR cmdLine = GetCommandLineW();                      // Retrieve the command line.
  int argc;
  LPWSTR* argv = CommandLineToArgvW(cmdLine, &argc);

  if (argc < 2) {
    std::wcerr << L"Usage: <program> <WindowTitle>" << std::endl;
    return 1;
  }

  const wchar_t* windowTitle = argv[1];

  HWND win = nullptr;
  if (::EnumWindows(EnumWindowsProc, (LPARAM)windowTitle) == 0 && win == 0) {
    std::wcerr << L"Error: Window not found." << std::endl;
    LocalFree(argv);                                       // Free memory allocated by CommandLineToArgvW.
    return 2;
  }

  BringWindowToTop(win);

  LocalFree(argv);                                         // Free memory allocated by CommandLineToArgvW.

  return 0;
}
