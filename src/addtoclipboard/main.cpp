// This tool allows extending the data currently stored in the clipboard.

#ifndef _UNICODE
#define _UNICODE
#endif
#ifndef UNICODE
#define UNICODE
#endif

#define WIN32_LEAN_AND_MEAN		       // Exclude rarely-used stuff from Windows headers.

#include <windows.h>
#include <shellapi.h>

#include <string>
#include <iostream>

//EmptyClipboard(); ?

std::wstring GetClipboardText()
{
  if (!OpenClipboard(nullptr)) {
    return L"";
  }

  HANDLE hData = GetClipboardData(CF_UNICODETEXT);
  if (hData == nullptr) {
    CloseClipboard();
    return L"";
  }

  WCHAR* pszText = static_cast<WCHAR*>(GlobalLock(hData));
  if (pszText == nullptr) {
    CloseClipboard();
    return L"";
  }

  std::wstring text(pszText);
  GlobalUnlock(hData);
  CloseClipboard();

  return text;
}

bool SetClipboardText(const std::wstring& text)
{
  if (!OpenClipboard(nullptr)) {
    return false;
  }

  if (!EmptyClipboard()) {
    CloseClipboard();
    return false;
  }

  size_t size = (text.size() + 1) * sizeof(WCHAR);
  HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE, size);
  if (hGlobal == nullptr) {
    CloseClipboard();
    return false;
  }

  WCHAR* pGlobal = static_cast<WCHAR*>(GlobalLock(hGlobal));
  memcpy(pGlobal, text.c_str(), size);
  GlobalUnlock(hGlobal);

  SetClipboardData(CF_UNICODETEXT, hGlobal);
  CloseClipboard();

  return true;
}

int main(int argcx, char *argvx[])
{
  LPCWSTR cmdLine = GetCommandLineW();                     // Kommandozeile abrufen.

  LPWSTR* argv;                                            // Überspringe den Programmnamen.
  int argc;
  argv = CommandLineToArgvW(cmdLine, &argc);

  if (argc < 2) {
    std::wcerr << L"Fehler: Keine Parameter angegeben." << std::endl;
    return 1;
  }

  std::wstring commandLine;                                // Parameter in eine Zeile zusammenfügen.
  for (int i = 1; i < argc; ++i) {
    if (i > 1) {
      commandLine += L" ";
    }
    commandLine += argv[i];
  }
  LocalFree(argv);

  std::wstring clipboardContent = GetClipboardText();      // Aktuellen Zwischenablageinhalt abrufen.

  // Parameter hinzufügen.

  std::wstring newClipboardContent = clipboardContent + (clipboardContent.empty() ? L"" : L"\n") + commandLine;

  if (SetClipboardText(newClipboardContent)) {             // Neuen Inhalt in die Zwischenablage setzen.
    std::wcout << L"Zwischenablage erfolgreich aktualisiert." << std::endl;
  } else {
    std::wcerr << L"Fehler beim Aktualisieren der Zwischenablage." << std::endl;
    return 2;
  }

  return 0;
}
