# addtoclipboard command

This tool defines adds its parameter string to the clipboard. This allows extending the currently stored clipboard data without loosing old entries.
