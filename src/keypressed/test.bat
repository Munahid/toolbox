@echo off
:go

REM https://stackoverflow.com/questions/10935693/foolproof-way-to-check-for-nonzero-error-return-code-in-windows-batch-file
REM https://superuser.com/questions/194662/how-to-check-the-exit-code-of-the-last-command-in-batch-file

(
  keypressed.exe
  set foundErr=1
  if errorlevel 0 if not errorlevel 1 set "foundErr="
  REM Output a message if errorlevel is non-zero
  if defined foundErr echo Key press detected.
)
goto go
