// This tool checks if a certain key or a key combination is pressed.

#ifndef _UNICODE
#define _UNICODE
#endif
#ifndef UNICODE
#define UNICODE
#endif

#define WIN32_LEAN_AND_MEAN                                // Exclude rarely-used stuff from Windows headers.

#include <windows.h>

#include <typeinfo>
#include <iostream>
#include <string>
#include <map>
#include <sstream>
#include <regex>
#include <algorithm>
#include <cwctype>
#include <cctype>
#include <unordered_map>

// https://learn.microsoft.com/de-de/windows/win32/inputdev/virtual-key-codes
// Virtual key codes from winuser.h

const std::unordered_map<std::string, int> keyNameTable = {   // Table for known key names.
  {"SHIFT",      VK_SHIFT},            // 16
  {"LCONTROL",   VK_LCONTROL},         // 162
  {"RCONTROL",   VK_RCONTROL},         // 163
  {"CONTROL",    VK_CONTROL},          // 17
  {"ALT",        VK_MENU},             // 18
  {"ESC",        VK_ESCAPE},           // 27
  {"ESCAPE",     VK_ESCAPE},           // 27
  {"CAPSLOCK",   VK_CAPITAL},          // 20
  {"NUMLOCK",    VK_NUMLOCK},          // 144
  {"SCROLLLOCK", VK_SCROLL},           // 145
  {"0",          0x30},                // Taste: 0
  {"1",          0x31},                // Taste: 1
  {"2",          0x32},                // Taste: 2
  {"3",          0x33},                // Taste: 3
  {"4",          0x34},                // Taste: 4
  {"5",          0x35},                // Taste: 5
  {"6",          0x36},                // Taste: 6
  {"7",          0x37},                // Taste: 7
  {"8",          0x38},                // Taste: 8
  {"9",          0x39},                // Taste: 9
  {"A",          0x41},                // Taste: A
  {"B",          0x42},                // Taste: B
  {"C",          0x43},                // Taste: C
  {"D",          0x44},                // Taste: D
  {"E",          0x45},                // Taste: E
  {"F",          0x46},                // Taste: F
  {"G",          0x47},                // Taste: G
  {"H",          0x48},                // Taste: H
  {"I",          0x49},                // Taste: I
  {"J",          0x4A},                // Taste: J
  {"K",          0x4B},                // Taste: K
  {"L",          0x4C},                // Taste: L
  {"M",          0x4D},                // Taste: M
  {"N",          0x4E},                // Taste: N
  {"O",          0x4F},                // Taste: O
  {"P",          0x50},                // Taste: P
  {"Q",          0x51},                // Taste: Q
  {"R",          0x52},                // Taste: R
  {"S",          0x53},                // Taste: S
  {"T",          0x54},                // Taste: T
  {"U",          0x55},                // Taste: U
  {"V",          0x56},                // Taste: V
  {"W",          0x57},                // Taste: W
  {"X",          0x58},                // Taste: X
  {"Y",          0x59},                // Taste: Y
  {"Z",          0x5A},                // Taste: Z
  {"F1",         VK_F1},               // 112
  {"F2",         VK_F2},
  {"F3",         VK_F3},
  {"F4",         VK_F4},
  {"F5",         VK_F5},
  {"F6",         VK_F6},
  {"F7",         VK_F7},
  {"F8",         VK_F8},
  {"F9",         VK_F9},
  {"F10",        VK_F10},
  {"F11",        VK_F11},
  {"F12",        VK_F12},              // 123
  {"LEFT",       VK_LEFT},
  {"RIGHT",      VK_RIGHT},
  {"UP",         VK_UP},
  {"DOWN",       VK_DOWN},
};

// Checks whether a key is currently pressed.

bool isKeyPressed(int key)
{
  return (GetAsyncKeyState(key) & 0x8000) != 0;
}

// Checks the toggle state of a key (e.g. Caps Lock, Num Lock).

bool isKeyToggled(int key)
{
  return (GetKeyState(key) & 0x0001) != 0;
}

// Converts a string into its lower-case version.
// No UNICODE, no umlauts.

std::string convertToLowerCase(const std::string& input)
{
  std::string result = input;
  for (char& c : result) {
    c = std::tolower(c);
  }

  //std::transform(str.begin(), str.end(), str.begin(),
  // [](unsigned char c){ return std::tolower(c); });
 
 return result;
}

// Function to replace key names in the string with decimal values.

std::string convertKeyNamesToDecimal(const std::string& input)
{
  std::string result = input;

  // \\b = only exact matches/words, no replace of XX in XXYY.

  for (const auto& [keyName, keyCode] : keyNameTable) {
    // Regex für exakte Übereinstimmung eines Schlüsselworts
    std::regex keyPattern("\\b" + keyName + "\\b", std::regex_constants::icase);
    result = std::regex_replace(result, keyPattern, std::to_string(keyCode));
  }

  return result;
}

// Converts from hexadecimal format into decimal values.

std::string convertHexToDecimal(const std::string& input)
{
  // Regex für das Muster 0xXX, wobei X eine Hexadezimalziffer ist
  std::regex hexPattern(R"(0x[0-9A-Fa-f]+)");

  // Ergebnisstring, in dem die Ersetzungen vorgenommen werden
  std::string result = input;

  // Iterator für alle Vorkommen von 0xXX
  std::sregex_iterator begin(input.begin(), input.end(), hexPattern);
  std::sregex_iterator end;

  for (std::sregex_iterator it = begin; it != end; ++it) {
    // Extrahiere den gefundenen Hex-String
    std::string hexStr = it->str();

    // Konvertiere den Hex-String in eine Dezimalzahl
    int decimalValue = std::stoi(hexStr, nullptr, 16);

    // Ersetze den Hex-String im Result-String durch die Dezimalzahl
    result.replace(result.find(hexStr), hexStr.length(), std::to_string(decimalValue));
  }

  return result;
}

// Read and handle a key with an optional binary operator.
// Returns the value of a toggle or key press check.

bool checkKey(const std::string& keyToken)
{
  bool result = false;                                     // Boolean value.
  int keyCode = 0;
  std::string token = keyToken;

  // Read and handle a key. Don't forget the binary operator.

  if (token.length() <= 0) {
    std::cout << "Error. token.length <= 0 in checkKey()." << std::endl;
    return false;
  }

  char c = 0;
  if (token.length() > 1) {
    c = token[0];
    if (c == '^' || c == '#' || c == '~') {
      token = token.substr(1);                             // Remove operator.
    }
    else {
      c = 0;
    }
  }

  // token should be a key, c is a valid operator or 0.

  keyCode = 0;
  auto it = keyNameTable.find(token);                      // Look up the KeyCode for the given key.
    
  if (it != keyNameTable.end()) {
    keyCode = it->second;
    std::cout << "KeyCode for " << token << ": " << keyCode << std::endl;
  } else {
    //std::cout << "Key name for token '" << token << "' not found!" << std::endl;
    try {
      keyCode = std::stoi(token, nullptr, 0);
    }
    catch (const std::invalid_argument& e) {
      std::cout << "Fehler: Der Eingabestring ist keine gültige Zahl. " << e.what() << std::endl;
    }
    catch (const std::out_of_range& e) {
      std::cout << "Fehler: Die Zahl ist zu groß oder zu klein. " << e.what() << std::endl;
    }
    catch (...) {
      std::cout << "Ein unbekannter Fehler ist aufgetreten." << std::endl;
    }
  }

  //std::cout << "Handle Key: Operator: " << c << " Token: " << token << std::endl;

  if (c == '^' || c == '#') {                              // A toggle operator.
    result = isKeyToggled(keyCode);
    if (c == '^')
      result = !result;
  }
  else {                                                   // Its a key press check.
    result = isKeyPressed(keyCode);
    if (c == '~')
      result = !result;
  }

  return result;
}

// Checks the expression.

// No brackets, well-known names, digits (keys), letters (keys), numbers
// (virtal key codes, hex/dec), binary operators directly to the left of
// the token (^ off, # on, <nothing> pressed, ~ not pressed), operators
// (+ AND and | OR, AND takes precedence over OR).

bool evaluateCombination(const std::string& combination)
{
  std::istringstream stream(combination);
  std::string token;
  bool firstLoop = true;
  bool result = false;
  bool pendingAnd = false;
  bool pendingOr = false;

  while (stream >> token) {
    std::cout << "Token: " << token << std::endl;

    // At the beginning we expect a key (name or decimal) optionally
    // with a binary operator.

    if (firstLoop) {
      result = checkKey(token);
      firstLoop = false;
      continue;
    }

    bool current = false;                                  // Current boolean value.

    if (pendingAnd || pendingOr) {
      current = checkKey(token);

      if (pendingAnd)
        result = result && current;
      else
        result = result || current;

      pendingAnd = false;
      pendingOr  = false;
      continue;
    }

    if (token == "+") {
      pendingAnd = true;
      continue;
    }

    if (token == "|") {
      pendingOr = true;
      continue;
    }
  }

  if (pendingAnd || pendingOr) {
    std::cout << "Error. AND or OR operator at the end of line (ignoring)." << std::endl;
  }

  return result;
}

// Main.

int main(int argc, char *argv[])
{
  // Exception: We do not want return != 0. A value of 1
  // means the requested key combination was recognized.

  if (argc < 2) {
    std::cout << "Usage: " << argv[0] << " <key (combination)>\n";
    return 0;
  }

  std::string input = argv[1];

  //std::cout << "Original: " << input << std::endl;

  input = convertKeyNamesToDecimal(input);       // Names first, because key names and digits are contained -> into hex value.
  input = convertToLowerCase(input);
  input = convertHexToDecimal(input);

  //std::cout << "Simplified: " << input << std::endl;

  if (evaluateCombination(input)) {
    std::cout << "Key combination was recognized." << std::endl;
    return 1;
  }

  std::cout << "Key combination was not recognized." << std::endl;

  return 0;
}
