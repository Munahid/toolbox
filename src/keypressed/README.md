# keypressed

This tool checks if a certain key or key combination is pressed during execution and returns 1 or 0 depending on the result.

For example, I used it in startup scripts when certain applications should not be loaded on a PC. For example, I used an automated login on my agency's time recording website to log in when I turned on the PC. If I had to restart the PC several times, I wanted to prevent these events from happening so that no questions would arise from the HR department. They want to log off every day for every correct login. And not 20 times in one day. So I could press Caps Lock when booting and the script automatically read the key status and skipped the login.


In Powershell you may use this snippet:

~~~
# Wenn Shift oder Ctrl gedrückt, dann Skript beenden.
$keypressed = Start-Process "\\nas\apps\portable\toolbox\bin\keypressed.exe <parameters>" -PassThru -Wait
echo "Taste: $($keypressed.ExitCode)"

# For example, if you want to abort the script in case a key is pressed:
if ($keypressed.ExitCode -eq 1) {
  Break
}
~~~

Within a batch file you may use:

~~~
(
  keypressed.exe "ESC"
  set foundErr=1
  if errorlevel 0 if not errorlevel 1 set "foundErr="
  REM Output a message if errorlevel is non-zero
  if defined foundErr echo Key press detected.
)
~~~

The batch to check the returned error code is a suggestion from [stackoverflow.com/Foolproof way to check for nonzero (error) return code in windows batch file](https://stackoverflow.com/questions/10935693/foolproof-way-to-check-for-nonzero-error-return-code-in-windows-batch-file) and [superuser.com/How to check the exit code of the last command in batch file?](https://superuser.com/questions/194662/how-to-check-the-exit-code-of-the-last-command-in-batch-file).

## Some hints

There are two ways to check whether a specific key is pressed or has recently been pressed in Windows. With GetAsyncKeyState and with GetKeyState.

GetAsyncKeyState works regardless of whether the program has the focus and the application window is active. It offers a real-time check. It can be queried whether the key is currently pressed and whether the key has been pressed since the last query (not in use at the moment). The query is done asynchronously (from the keyboard queue), with the key queue being accessed system-wide, which could lead to performance problems if queried very frequently.

GetKeyState checks the state of a key in relation to the message loop of the current thread. It only works for programs with focus. The query is synchronous, i.e. it is tied to the processing of messages in the application. It can be checked whether a key is pressed and the toggle state of keys such as Caps Lock, Scroll Lock or Num Lock can be queried. This is possible because these keys have an on/off functionality. The documentation states that the associated bit (which we are querying here) is generally not relevant for other keys. This means that there may be other keys with additional functions. I am thinking of Lenovo's speaker mute function.

We use GetKeyState für all toggle key checks and GetAsyncKeyState for is this key pressed checks.

## Parameters

Names such as SHIFT are converted into virtual key codes using an internal table. Decimal or hexadecimal values ​​(e.g. 123, 0xAB) are interpreted directly as key codes. OR ('|') and AND ('+') combinations of keys can be created. Keys that are not specified are treated as not pressed and are not queried at all. A corresponding query is currently not functional ('~'). Using a preceding marker '#', the toggle state can be queried for ON and with '^' for OFF.

## Examples

~~~
keypressed.exe "SHIFT + LCONTROL"
keypressed.exe "SHIFT | 0xAB"
keypressed.exe "#CAPSLOCK + ^NUMLOCK"
keypressed.exe "SHIFT + #CAPSLOCK + ^NUMLOCK"
keypressed.exe "F1 | ESC | SHIFT + LCONTROL"
keypressed.exe "SHIFT + ~LCONTROL"
keypressed.exe "A + S + D + F"
keypressed.exe "#CAPSLOCK + ~SHIFT"
keypressed.exe "^CAPSLOCK + SHIFT"
~~~

[A list with possible key codes.](https://learn.microsoft.com/de-de/windows/win32/inputdev/virtual-key-codes) Not all are available.
