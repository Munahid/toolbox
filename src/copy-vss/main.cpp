//

#define UNICODE
#define _UNICODE

#include <windows.h>
#include <vss.h>
#include <vswriter.h>
#include <vsbackup.h>
#include <comdef.h>

#include <iostream>
#include <string>
#include <vector>
#include <sstream>

// There is no CComPtr<> in MinGW, because it belongs to
// Visual Studio's ctive Template Library (ATL).
// This is a minimal replacement.

template <typename T>
class ComPtr {
public:
    ComPtr() : ptr(nullptr) {}
    ~ComPtr() { reset(); }

    T** operator&() { return &ptr; }
    T* operator->() { return ptr; }
    T* get() const { return ptr; }

    void reset() {
        if (ptr) {
            ptr->Release();
            ptr = nullptr;
        }
    }

    // Übernahme eines Zeigers
    void reset(T* newPtr) {
        reset();
        ptr = newPtr;
    }

    // Kopieren nicht erlaubt
    ComPtr(const ComPtr&) = delete;
    ComPtr& operator=(const ComPtr&) = delete;

    // Bewegung erlaubt
    ComPtr(ComPtr&& other) noexcept : ptr(other.ptr) { other.ptr = nullptr; }
    ComPtr& operator=(ComPtr&& other) noexcept {
        if (this != &other) {
            reset();
            ptr = other.ptr;
            other.ptr = nullptr;
        }
        return *this;
    }

private:
    T* ptr;
};

bool CopyLockedFileWithVSS(const std::wstring& source, const std::wstring& destination)
{
    HRESULT hr = S_OK;
    ComPtr<IVssBackupComponents> vssBackup;
    VSS_ID snapshotSetId = {};
    VSS_ID snapshotId = {};

    try {
        // Initialisiere COM
        hr = CoInitialize(nullptr);
        if (FAILED(hr)) throw _com_error(hr);

        // Erstelle VSS Backup Components
        hr = CreateVssBackupComponents(vssBackup.operator&());
        if (FAILED(hr)) throw _com_error(hr);

        // Initialisiere VSS
        hr = vssBackup->InitializeForBackup();
        if (FAILED(hr)) throw _com_error(hr);

        // Snapshot-Set erstellen
        hr = vssBackup->StartSnapshotSet(&snapshotSetId);
        if (FAILED(hr)) throw _com_error(hr);

        // Volume der Quelle hinzufügen
        std::wstring volume = source.substr(0, source.find(L'\\') + 1);
        hr = vssBackup->AddToSnapshotSet(const_cast<wchar_t*>(volume.c_str()), GUID_NULL, &snapshotId);
        if (FAILED(hr)) throw _com_error(hr);

        // Snapshot erstellen
        //hr = vssBackup->DoSnapshotSet(nullptr, nullptr);
        //if (FAILED(hr)) throw _com_error(hr);

ComPtr<IVssAsync> async;
hr = vssBackup->DoSnapshotSet(async.operator&());
if (FAILED(hr)) throw _com_error(hr);

// Optional: Auf den Abschluss warten (falls benötigt)
if (async.get()) {
    hr = async->Wait();
    if (FAILED(hr)) throw _com_error(hr);
}

        // Hole den Snapshot-Pfad
        VSS_SNAPSHOT_PROP snapshotProp = {};
        hr = vssBackup->GetSnapshotProperties(snapshotId, &snapshotProp);
        if (FAILED(hr)) throw _com_error(hr);

        std::wstring snapshotPath = snapshotProp.m_pwszSnapshotDeviceObject;
        VssFreeSnapshotProperties(&snapshotProp);

        // Erstelle den vollständigen Quellpfad aus dem Snapshot
        std::wstring snapshotSource = snapshotPath + source.substr(volume.size() - 1);

        // Kopiere die Datei
        if (!CopyFileW(snapshotSource.c_str(), destination.c_str(), FALSE)) {
            std::wcerr << L"Fehler beim Kopieren der Datei: " << GetLastError() << std::endl;
            return false;
        }

        std::wcout << L"Datei erfolgreich kopiert: " << snapshotSource << L" -> " << destination << std::endl;
    } catch (const _com_error& e) {
        std::wcerr << L"COM-Fehler: " << e.ErrorMessage() << std::endl;
        if (vssBackup.get()) vssBackup->AbortBackup();
        return false;
    }

    // COM freigeben
    CoUninitialize();
    return true;
}

std::vector<std::wstring> GetCommandLineArguments()
{
  int argc = 0;
  LPWSTR* argv = CommandLineToArgvW(GetCommandLineW(), &argc);

  std::vector<std::wstring> arguments;
  if (argv) {
    for (int i = 0; i < argc; ++i) {
      arguments.emplace_back(argv[i]);
    }
    LocalFree(argv);
  }

  return arguments;
}

int main(int argc, const char* argv[])
{
  auto arguments = GetCommandLineArguments();

  if (arguments.size() != 3) {
    std::wcerr << L"Usage: " << arguments[0] << L" <source> <destination>" << std::endl;
    return 1;
  }

  std::wstring source = arguments[1];
  std::wstring destination = arguments[2];

  if (CopyLockedFileWithVSS(source, destination)) {
    std::wcout << L"File copied successfully!" << std::endl;
  } else {
    std::wcerr << L"File could not be copied." << std::endl;
  }

  return 0;
}
