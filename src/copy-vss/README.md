# copy-vss command

Dieser Befehl kopiert eine gesperrte Datei, indem eine Schattenkopie erstellt und die Datei aus dieser kopiert wird. Das Programm muss mit Administratorrechten ausgeführt werden, da VSS nur auf hoher Privilegstufe funktioniert.

Dazu wird der Volume Shadow Copy Service (VSS) verwendet und eine IVssBackupComponents-Schnittstelle erstellt. Über diese wird ein Snapshot-Set gestartet, und das Quelllaufwerk hinzugefügt. Es wird ein Snapshot des Volumes erstellt und der Pfad des Snapshot ermittelt, um die Datei im schreibgeschützten Zustand mittels CopyFile aus dem Snapshot zu kopieren. Anschließend werden alle Ressourcen wieder ordnungsgemäß freigegeben.


This command copies a locked file by creating a shadow copy and copying the file from it. The program must be run with administrator rights because VSS only works at high privilege levels.

This uses the Volume Shadow Copy Service (VSS) and creates an IVssBackupComponents interface. This is used to start a snapshot set and add the source drive. A snapshot of the volume is created and the path of the snapshot is determined in order to copy the file from the snapshot in a read-only state using CopyFile. All resources are then released properly.
