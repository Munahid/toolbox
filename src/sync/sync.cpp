// Synchronizes all drives and writes cached data onto them.

#ifndef _UNICODE
#define _UNICODE
#endif
#ifndef UNICODE
#define UNICODE
#endif

#define WIN32_LEAN_AND_MEAN		       // Exclude rarely-used stuff from Windows headers.

#include <windows.h>

#include <iostream>
#include <algorithm>    // std::find_if
//include <stdlib.h>
#include <strsafe.h>

void ShowGetLastErrorMessage(bool showDialog = false)
{
  LPCTSTR lpszFunction = L"";

  LPVOID lpMsgBuf;
  LPVOID lpDisplayBuf;
  DWORD dw = GetLastError();

  FormatMessage(
    FORMAT_MESSAGE_ALLOCATE_BUFFER | 
    FORMAT_MESSAGE_FROM_SYSTEM | 
    FORMAT_MESSAGE_IGNORE_INSERTS,
    NULL,
    dw,
    MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),             // 0: Default language.
    (LPTSTR) &lpMsgBuf,
    0,
    NULL);

  // Process any inserts in lpMsgBuf.

  lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT, 
        (lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) * sizeof(TCHAR)); 
  StringCchPrintf((LPTSTR)lpDisplayBuf, 
        LocalSize(lpDisplayBuf) / sizeof(TCHAR),
        L"%s failed with error %d: %s", 
        lpszFunction, dw, lpMsgBuf);

  if (showDialog) {
    MessageBox(NULL, (LPCTSTR)lpMsgBuf, L"Error", MB_OK | MB_ICONINFORMATION);
    MessageBox(NULL, (LPCTSTR)lpDisplayBuf, L"Error", MB_OK);
  }	else {
    std::wcout << L" error" << std::endl;                  // TODO.
  }

  LocalFree(lpMsgBuf);
}

int main(int argc, char *argv[])
{
  HANDLE volume;
  DWORD attributes;
  BOOL rc;
  std::wstring ws;
  wchar_t driveName[] = {'\\', '\\', '.', '\\', 'A', ':', '\0'};

  std::wcout << L"Sync" << std::endl;

  // Bitmask representing the currently available disk drives.
  // Bit position 0 is drive A, and so on.

  DWORD drives = ::GetLogicalDrives();

  if (drives == 0) {
    std::wcout << L"Error: " << ::GetLastError() << std::endl;
    return 1;
  }

  wchar_t driveRoot[] = L"1234567";    // For a string like L"C:\\".
  char    driveLetter = 'A';           // Start with A and increase with every loop.

  while (drives) {                     // There are bits set, a drive must exists.
    if (drives & 0x1L) {               // Is the least significant bit set? Found a drive.
      swprintf_s(driveRoot, 6, L"%c:\\",      driveLetter);
      swprintf_s(driveName, 7, L"\\\\.\\%c:", driveLetter); // Only one \ instead of \\?

      std::wcout << L"Processing drive: " << driveLetter << " -> ";

      // Check if drive is flushable (has the right type).

      UINT type = ::GetDriveType(driveRoot);

      switch (type) {
        case DRIVE_UNKNOWN:     std::wcout << "Unknown drive"; break;
        case DRIVE_NO_ROOT_DIR: std::wcout << "Invalid path/not mounted"; break;
        case DRIVE_REMOVABLE:   std::wcout << "Removable media"; break;
        case DRIVE_FIXED:       std::wcout << "Fixed hard drive"; break;
        case DRIVE_REMOTE:      std::wcout << "Remote (network) drive"; break;
        case DRIVE_CDROM:       std::wcout << "CD-ROM drive"; break;
        case DRIVE_RAMDISK:     std::wcout << "RAM disk"; break;
      }

      if ((type == DRIVE_CDROM)   | (type == DRIVE_RAMDISK) |
          (type == DRIVE_UNKNOWN) | (type == DRIVE_NO_ROOT_DIR))
        goto nextdrive;   // Dislike nested if statement here.

      // Check if drive contains a medium.

      // Calling GetVolumeInformation() will make floppy drives produce
      // some noise no matter if a disk is inserted or not. Use more
      // advanced storage APIs to first filter out floppy drives?

      DWORD filesystemflags;
      WCHAR filesystemname[20];

      ::SetErrorMode(SEM_FAILCRITICALERRORS);  // Do not ask if media is not present.
      rc = ::GetVolumeInformation(driveRoot, NULL, 0, NULL, NULL, &filesystemflags,
                                  filesystemname, 20);
      ::SetErrorMode(0);

      if (rc == 0) {
        // GetLastError!
        std::wcout << " -> no media";
        goto nextdrive;
      }

      // https://docs.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-getvolumeinformationw

      ws = L"";

      // The specified volume supports Unicode in file names.
      if (filesystemflags & FILE_UNICODE_ON_DISK)
        ws += L" unicode";
      // The specified volume is read-only.
      if (filesystemflags & FILE_READ_ONLY_VOLUME)
        ws += L" ro";
      // The specified volume supports the Encrypted File System (EFS).
      if (filesystemflags & FILE_SUPPORTS_ENCRYPTION)
        ws += L" efs";
      // The specified volume supports case-sensitive file names.
      if (filesystemflags & FILE_CASE_SENSITIVE_SEARCH)
        ws += L" cs";

      // google: std::string strip leading space
      // https://stackoverflow.com/questions/216823/how-to-trim-a-stdstring
      // left trim
      ws.erase(ws.begin(), std::find_if(ws.begin(), ws.end(), [](unsigned char ch) {
        return !std::isspace(ch);
      }));

      std::wcout << " (" << ws << ")";

      // Check if the permissions are okay (if drive is writeable).

      attributes = ::GetFileAttributes(driveName);

      if (attributes == INVALID_FILE_ATTRIBUTES) {
				// Some drives have Invalid Attributes and are able to flush. Why?
				// Does this mean this function is only for files? Why can I then
				// check against the floppy drive and have not this return value?
				//printf(" -> invld attrs ");
				//goto nextdrive;
      }
      else {
        if (attributes & FILE_ATTRIBUTE_READONLY) {
          std::wcout << " -> readonly";
          goto nextdrive;          // Syncing not needed.
        }
      }

      // Flush the drive.

      std::wcout << " -> flushing";

      volume = ::CreateFile(driveName, GENERIC_READ | GENERIC_WRITE,
                            FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
                            OPEN_EXISTING, 0, NULL);

      if (volume == INVALID_HANDLE_VALUE) {
        std::wcout << " error: vol";   // Zugriff verweigert?
        volume = 0;

        ShowGetLastErrorMessage();

        CloseHandle(volume); // ?

        goto nextdrive;

        //return -1;

        // Under Windows NT CreateFile() fails and GetLastError()
        // returns "The parameter is incorrect." rather than
        // something sane like "File/Device not found.".
      }

      rc = ::FlushFileBuffers(volume);

      if (rc == 0) {
        std::wcout << " error: flush";

        ShowGetLastErrorMessage();

		    //return -2;
      }

      ::CloseHandle(volume);

nextdrive:

      std::wcout << std::endl;
    }
    drives >>= 1;                      // Next drive is next bit.
    driveLetter++;
  }
}
