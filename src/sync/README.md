sync command

This command flushes the windows buffers of your drives to ensure all unwritten data is on your hard disk. This is a rewrite of Mark Russinovich's sync command from his well-known website https://www.sysinternals.com/ because he didn't provided the source code for this command.
