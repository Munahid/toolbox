# isadmin command

This is a command line application which allows a script asking if it is running as Administrator without checking only against the user's login name. The isadmin tool simply returns a value whether the process is running with administrator rights or not.
