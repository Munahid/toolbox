//

#ifndef UNICODE
#define UNICODE
#endif 
#ifndef _UNICODE
#define _UNICODE
#endif 

#define WIN32_LEAN_AND_MEAN     // Exclude rarely-used stuff from Windows headers

#include <windows.h>

#include <iostream>

int main(int argc, char *argv[])
{
  // Überprüfen, ob die Anwendung mit Administratorrechten ausgeführt wird.
  BOOL isAdmin = FALSE;
  SID_IDENTIFIER_AUTHORITY NtAuthority = SECURITY_NT_AUTHORITY;
  PSID AdministratorsGroup; 

  // Überprüfen, ob der aktuelle Benutzer zur Admin-Gruppe gehört
  if (AllocateAndInitializeSid(&NtAuthority, 2,
                               SECURITY_BUILTIN_DOMAIN_RID,
                               DOMAIN_ALIAS_RID_ADMINS,
                               0, 0, 0, 0, 0, 0,
                               &AdministratorsGroup)) {
    CheckTokenMembership(NULL, AdministratorsGroup, &isAdmin);
    FreeSid(AdministratorsGroup);
  }

  if (isAdmin) {
    std::wcout << L"This application runs as Administrator." << std::endl;
    return 1;
  }

  std::wcout << L"This application runs without administrator rights." << std::endl;

  return 0;
}
