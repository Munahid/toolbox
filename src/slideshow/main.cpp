// slideshow/main.cpp

// Buggy code. Segfault bei Mausklick. TODO.

#define UNICODE
#define _UNICODE

#include <windows.h>
#include <mfapi.h>
#include <mfobjects.h>
#include <mferror.h>
#include <mfidl.h>
#include <commdlg.h>
#include <gdiplus.h>

#include <string>
#include <vector>
#include <filesystem>
#include <iostream>
#include <unordered_map>
#include <deque>
#include <memory>

using namespace std::filesystem;
using namespace Gdiplus;

#if 0
class ImageCache {
public:
    ImageCache(size_t maxSize) {
      std::wcout << "ic construct" << std::endl;
    }

   ~ImageCache() {
      std::wcout << "ic destruct" << std::endl;
    }

    std::shared_ptr<Image> GetImage(const std::wstring& path) {
        std::shared_ptr<Image> newImage = std::make_shared<Image>(path.c_str());
        if (newImage->GetLastStatus() != Ok) {
            return nullptr; // Fehler beim Laden des Bildes.
        }

        return newImage;
    }
};
#endif

#if 1
class ImageCache {
public:
    ImageCache(size_t maxSizeX) : maxSize(maxSizeX), imageMap({}), imageOrder({}) {  // X added, due to -Werror=shadow.
      std::wcout << "ic construct" << std::endl;
    }

   ~ImageCache() {
std::wcout << "ic destruct" << std::endl;
        // Hier ggf. Ressourcen freigeben.
        // Nutzen std::shared_ptr, daher eigentlich nicht erforderlich.
    }

    // Gibt das nächste Bild zurück oder lädt es, wenn es noch nicht im Cache ist.
    std::shared_ptr<Image> GetImage(const std::wstring& path) {
        // Überprüfen, ob das Bild bereits im Cache ist.
        auto it = imageMap.find(path);
        if (it != imageMap.end()) {
            return it->second;            // Bild gefunden, zurückgeben.
        }

        // Bild nicht im Cache, laden.
        std::shared_ptr<Image> newImage = std::make_shared<Image>(path.c_str());
        if (newImage->GetLastStatus() != Ok) {
            return nullptr; // Fehler beim Laden des Bildes.
        }

        // Bild im Cache speichern.
        imageMap[path] = newImage;
        imageOrder.push_back(path); // Hinzufügen zur Reihenfolge.

        // Cache verwalten.
        if (imageOrder.size() > maxSize) {
            // Ältestes Bild entfernen
            const std::wstring& oldestPath = imageOrder.front();
            imageOrder.pop_front();
            imageMap.erase(oldestPath);
        }

        return newImage;
    }

private:
    size_t maxSize;                                        // Maximale Größe des Caches.
    std::unordered_map<std::wstring, std::shared_ptr<Image>> imageMap;  // HashMap for loaded images.
    std::deque<std::wstring> imageOrder;                   // Reihenfolge der geladenen Bilder.
};
#endif

// Prototypes.

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
void DrawImage(HWND hwnd);
void LoadImagesFromDirectory(const std::wstring& directory, std::vector<std::wstring>& images);
void StartSlideshow(HWND hwnd, const std::vector<std::wstring>& images);

// Constants.

const UINT TIMER_NEXT_IMAGE = 1;       // ID für den 5-Sekunden-Timer.
const UINT TIMER_FADE_INOUT = 2;       // ID für den 10-ms-Timer.

// Global variables.

int timerFadeInOutCount = 0;           // Zähler für den Fade In/Out Timer.

std::vector<std::wstring> g_images;
size_t g_currentImageIndex = 0;
HWND g_hwnd;

size_t g_nextImageIndex = 0;
int    g_alpha = 255;                  // Alpha-Wert für die Überblendung.
bool   g_fadingOut = true;             // Flag für die Überblendung.

//ImageCache g_cache(10);                // Cache für die letzten 10 Bilder.
ImageCache *g_cache = nullptr;

// Error message box.

void ShowErrorMessageBox(const wchar_t message[])
{
  MessageBox(NULL, message, L"Error:", MB_OK);
}

// Main entry point.

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR lpCmdLine, int nCmdShow)
{
  GdiplusStartupInput gdiplusStartupInput;
  ULONG_PTR gdiplusToken;
  GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

  if (FAILED(MFStartup(MF_VERSION))) {
    ShowErrorMessageBox(L"Media Foundation startup failed.");
    return 1;
  }

  int argc;                                                // Befehlszeilenparameter abrufen.
  LPWSTR* argv = CommandLineToArgvW(GetCommandLineW(), &argc);
  if (argc < 2) {
    ShowErrorMessageBox(L"Bitte geben Sie ein Verzeichnis als Parameter an.");
    return 2;
  }

  std::wstring directory = argv[1];

  LoadImagesFromDirectory(directory, g_images);
  if (g_images.empty()) {
    ShowErrorMessageBox(L"Keine Bilder im angegebenen Verzeichnis gefunden.");
    return 3;
  }

  const wchar_t className[] = L"GASI Slideshow Window Class";

  WNDCLASS wc = {};
  wc.lpfnWndProc   = WindowProc;
  wc.hInstance     = hInstance;
  wc.lpszClassName = className;

  RegisterClass(&wc);

  ImageCache cache(10);                // Cache für die letzten 10 Bilder.
  g_cache = &cache;

  g_hwnd = CreateWindowEx(
        WS_EX_TOPMOST,
        className,
        L"Slideshow",
        WS_POPUP,
        0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN),
        NULL,
        NULL,
        hInstance,
        NULL);

  if (g_hwnd == NULL) {
    ShowErrorMessageBox(L"Couldn't open the window.");
    return 4;
  }

  ShowWindow(g_hwnd, nCmdShow);
  ShowCursor(FALSE);                                       // Hide mouse pointer.
  //SetForegroundWindow(g_hwnd);
  StartSlideshow(g_hwnd, g_images);

  MSG msg;
  while (GetMessage(&msg, NULL, 0, 0)) {
    TranslateMessage(&msg);
    DispatchMessage(&msg);
  }

  MFShutdown();
  GdiplusShutdown(gdiplusToken);

  return 0;
}

// Search for valid images and store the paths.

void LoadImagesFromDirectory(const std::wstring& directory, std::vector<std::wstring>& images)
{
  for (const auto& entry : directory_iterator(directory)) {
    if (entry.is_regular_file() && (entry.path().extension() == L".jpg" ||
                                    entry.path().extension() == L".png" ||
                                    entry.path().extension() == L".bmp")) {
      images.push_back(entry.path().wstring());
    }
  }
}

// Start the show by setting the image change timer.

void StartSlideshow(HWND hwnd, const std::vector<std::wstring>& images)
{
  SetTimer(hwnd, TIMER_NEXT_IMAGE, 5000, NULL);            // Timer für 5 Sekunden.
}

// Request a new drawing.

void DrawImage(HWND hwnd)
{
  InvalidateRect(hwnd, NULL, TRUE);                        // Fenster neu zeichnen.
}

// Window procedure.

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
  switch (uMsg) {
    case WM_TIMER:
//      if (wParam == TIMER_FADE_INOUT) {
//        timerFadeInOutCount++;

//        if (timerFadeInOutCount >= 10) {
//          KillTimer(hwnd, TIMER_FADE_INOUT);
//          std::wcout << L"25-ms-Timer gestoppt nach 10 Auslösungen." << std::endl;
//          //timerFadeInOutCount = 0;
//        }
//      }
//      else
    if (wParam == TIMER_NEXT_IMAGE) {
        g_currentImageIndex = (g_currentImageIndex + 1) % g_images.size();
        DrawImage(hwnd);
      }
      break;

    case WM_PAINT: {
        PAINTSTRUCT ps;
        HDC hdc = BeginPaint(hwnd, &ps);
        Graphics graphics(hdc);

        //graphics.Clear(Color(255, 255, 255));              // Clear background.

        //SetTimer(hwnd, TIMER_FADE_INOUT, 25, NULL); // 25ms and alpha range 0..255, 10 images for fading.

        // Load image.
        std::shared_ptr<Image> image = g_cache->GetImage(g_images[g_currentImageIndex].c_str());

        if (image) {
          //Image& imageRef = *image;

          if (image->GetLastStatus() == Ok) {
            // Draw image in fullscreen mode.
            graphics.DrawImage(image.get(), 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));
          }
        }
        else {
          std::wcerr << L"Failed to load image." << std::endl;
        }

        EndPaint(hwnd, &ps);
        break;
    }

#if 0
// Fading klappt noch nicht. TODO

    case WM_TIMER:
        if (g_fadingOut) {
            g_alpha -= 5; // Alpha-Wert verringern
            if (g_alpha <= 0) {
                g_alpha = 0;
                g_fadingOut = false;
                g_currentImageIndex = g_nextImageIndex; // Aktuelles Bild aktualisieren
                g_nextImageIndex = (g_currentImageIndex + 1) % g_images.size(); // Nächstes Bild festlegen
            }
        } else {
            g_alpha += 5; // Alpha-Wert erhöhen
            if (g_alpha >= 255) {
                g_alpha = 255;
                g_fadingOut = true; // Zurück zur Überblendung
            }
        }
        DrawImage(hwnd);
        break;

    case WM_PAINT: {
        PAINTSTRUCT ps;
        HDC hdc = BeginPaint(hwnd, &ps);
        Graphics graphics(hdc);

        // Aktuelles Bild laden und zeichnen
        Image currentImage(g_images[g_currentImageIndex].c_str());
        Image nextImage(g_images[g_nextImageIndex].c_str());

        if (currentImage.GetLastStatus() == Ok) {
            // Aktuelles Bild zeichnen
            graphics.DrawImage(&currentImage, 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));
        }

        if (nextImage.GetLastStatus() == Ok) {
            // Nächstes Bild zeichnen
            // Erstellen eines neuen Bitmap mit Alpha-Kanal
            Bitmap bitmap(GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), PixelFormat32bppARGB);
            Graphics bitmapGraphics(&bitmap);
            bitmapGraphics.Clear(Color(0, 0, 0, 0)); // Transparenten Hintergrund

            // Nächstes Bild zeichnen
            bitmapGraphics.DrawImage(&nextImage, 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));

            // Überblendung anwenden
            for (int y = 0; y < bitmap.GetHeight(); y++) {
                for (int x = 0; x < bitmap.GetWidth(); x++) {
                    Color pixelColor;
                    bitmap.GetPixel(x, y, &pixelColor);
                    Color newColor(pixelColor.GetAlpha() * g_alpha / 255, pixelColor.GetRed(), pixelColor.GetGreen(), pixelColor.GetBlue());
                    bitmap.SetPixel(x, y, newColor);
                }
            }

            // Zeichnen des überblendeten Bildes
            graphics.DrawImage(&bitmap, 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));
        }

        EndPaint(hwnd, &ps);
        break;
    }
#endif

    case WM_KEYDOWN:
      if (wParam == VK_ESCAPE || wParam == VK_SPACE) {
        PostMessage(hwnd, WM_DESTROY, 0, 0);
      }
      break;

    case WM_LBUTTONDOWN:
      PostMessage(hwnd, WM_DESTROY, 0, 0);
      break;

    case WM_DESTROY:
      ShowCursor(TRUE);                                    // Show mouse pointer again.
      PostQuitMessage(0);
      return 0;
  }

  return DefWindowProc(hwnd, uMsg, wParam, lParam);
}
