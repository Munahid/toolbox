# copy-backup command

Dieser Befehl kopiert eine gesperrte Datei mit CreateFile im Backup-Modus. Dazu wird das BACKUP_SEMANTICS-Flag gesetzt, womit Dateien geöffnet werden können, die von anderen Prozessen gesperrt sind. Dieser Ansatz benötigt Administratorrechte.

This command copies a locked file using CreateFile in backup mode. This is done by setting the BACKUP_SEMANTICS flag, which allows opening files locked by other processes. This approach requires administrator privileges.

