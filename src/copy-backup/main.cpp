//

#define UNICODE
#define _UNICODE

#include <windows.h>
#include <iostream>
#include <string>
#include <vector>

bool CopyLockedFile(const std::wstring& source, const std::wstring& destination)
{
  HANDLE hFile = CreateFile(                 // Open the locked file in backup mode.
        source.c_str(),
        GENERIC_READ,
        FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
        nullptr,
        OPEN_EXISTING,
        FILE_FLAG_BACKUP_SEMANTICS,
        nullptr);

  if (hFile == INVALID_HANDLE_VALUE) {
    std::wcerr << L"Error while opening the file: " << GetLastError() << std::endl;
    return false;
  }

  HANDLE hDest = CreateFile(   // Create destination file.
        destination.c_str(),
        GENERIC_WRITE,
        0,
        nullptr,
        CREATE_ALWAYS,
        FILE_ATTRIBUTE_NORMAL,
        nullptr);

  if (hDest == INVALID_HANDLE_VALUE) {
    std::wcerr << L"Error creating the target file: " << GetLastError() << std::endl;
    CloseHandle(hFile);
    return false;
  }

  char buffer[4096];                      // Copy file contents.
  DWORD bytesRead, bytesWritten;
  bool success = true;

  while (ReadFile(hFile, buffer, sizeof(buffer), &bytesRead, nullptr) && bytesRead > 0) {
    if (!WriteFile(hDest, buffer, bytesRead, &bytesWritten, nullptr) || bytesRead != bytesWritten) {
      std::wcerr << L"Fehler beim Schreiben: " << GetLastError() << std::endl;
      success = false;
      break;
    }
  }

  if (!success) {
    std::wcerr << L"Fehler beim Kopieren der Datei." << std::endl;
  }

  CloseHandle(hFile);              // Close handles.
  CloseHandle(hDest);

  return success;
}

std::vector<std::wstring> GetCommandLineArguments()
{
  int argc = 0;
  LPWSTR* argv = CommandLineToArgvW(GetCommandLineW(), &argc);

  std::vector<std::wstring> arguments;
  if (argv) {
    for (int i = 0; i < argc; ++i) {
      arguments.emplace_back(argv[i]);
    }
    LocalFree(argv);
  }

  return arguments;
}

int main(int argc, const char* argv[])
{
  //if (argc != 3) {
  //  std::wcerr << L"Usage: " << argv[0] << L" <source> <destination>" << std::endl;
  //  return 1;
  //}

  auto arguments = GetCommandLineArguments();

  if (arguments.size() != 3) {
    std::wcerr << L"Usage: " << arguments[0] << L" <source> <destination>" << std::endl;
    return 1;
  }

  //std::wstring source = argv[1];
  //std::wstring destination = argv[2];
  std::wstring source = arguments[1];
  std::wstring destination = arguments[2];

  if (CopyLockedFile(source, destination)) {
    std::wcout << L"File copied successfully!" << std::endl;
  } else {
    std::wcerr << L"File could not be copied." << std::endl;
  }

  return 0;
}
