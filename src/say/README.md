# say command

Already in the 1990s there was a "say" command on the [Amiga 500](https://de.wikipedia.org/wiki/Amiga_500) computer that could output a text in spoken form. Such a command is now, 30 years later, also available for the Windows PC. Just type "say hello" and your computer will say "hello" to you.
