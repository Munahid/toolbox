// This tool talks to you.

#ifndef _UNICODE
#define _UNICODE
#endif
#ifndef UNICODE
#define UNICODE
#endif

#define WIN32_LEAN_AND_MEAN		       // Exclude rarely-used stuff from Windows headers.

#include <windows.h>
#include <sapi.h>

#include <string>

int main(int argc, char* argv[])
{
  if (argc != 2) {
    return 1;
  }

  std::string str = { argv[1] };
  std::wstring ws = std::wstring(str.begin(), str.end());

  ISpVoice *voice = NULL;              // The voice interface.

  if (FAILED(CoInitialize(NULL))) {    // Initialize COM.  CoInitializeEx(NULL, COINIT_MULTITHREADED);
    return 2;
  }

  HRESULT hr = CoCreateInstance(CLSID_SpVoice, NULL, CLSCTX_ALL, IID_ISpVoice, (void **)&voice);

  if (SUCCEEDED(hr)) {
    hr = voice->Speak(ws.c_str(), SPF_DEFAULT, NULL);   // Voice->lpVtbl->Speak(pVoice,  texte, SPF_ASYNC|SPF_IS_XML, NULL)){
    if (voice) {
      voice->Release();
      voice = NULL;
    }
  }

  CoUninitialize();

  return 0;
}
