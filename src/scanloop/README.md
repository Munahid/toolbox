# scanloop command

This is a command line application which scans images and saves them into a directory. I use it because my Lexmark printer/scanner needs a lot of time to navigate through the menus and then it always asks if I'm finished or it uses the wrong source port because while starting the job the other source was needed.

