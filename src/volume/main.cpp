//

#define UNICODE
#define _UNICODE

#include <windows.h>
#include <mmdeviceapi.h>
#include <endpointvolume.h>

#include <iostream>
#include <cstdlib>                 // atoi
#include <stdexcept>               // std::invalid_argument
#include <string>

int main(int argc, char *argv[])
{
  HRESULT hr;
  CoInitialize(NULL);              // Initialize COM.

  if (argc > 2) {
    std::wcerr << L"Error: Too many parameters. Exit." << std::endl;
    return 1;
  }

  // Either a volume or a mute status can be set.

  bool volume_set   = false;   // Parameter is a volume value. Set it.
  int  volume_value = 0;
  bool mute_set     = false;   // Parameter is a mute value. Set it.
  bool mute_value   = false;

  if (argc == 2) {
    try {
      std::string argument = argv[1];

      if (argument == "mute") {
        mute_set   = true;
        mute_value = true;
      } else if (argument == "unmute") {
        mute_set   = true;
        mute_value = false;
      } else {
        volume_set = true;
        volume_value = std::stoi(argv[1]);

        if (volume_value < 0 || volume_value > 100) {
          std::wcerr << L"Error: The value must be between 0 and 100." << std::endl;
          return 2;
        }
      }
    }
    catch (const std::invalid_argument&) {
      std::wcerr << "Error: Invalid number." << std::endl;
      return 3;
    }
    catch (const std::out_of_range&) {
      std::wcerr << L"Error: Number outside the valid range." << std::endl;
      return 4;
    }
  }

  // Create device enumerator.
  IMMDeviceEnumerator *deviceEnumerator = NULL;
  hr = CoCreateInstance(__uuidof(MMDeviceEnumerator), NULL, CLSCTX_INPROC_SERVER,
                        __uuidof(IMMDeviceEnumerator), (LPVOID *)&deviceEnumerator);
  if (FAILED(hr)) {
    std::wcerr << L"Fehler beim Erstellen des Device Enumerators." << std::endl;
    return 5;
  }

  // Standard-Audioendpunktgerät (Lautsprecher) abrufen.
  IMMDevice *defaultDevice = NULL;
  hr = deviceEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, &defaultDevice);
  deviceEnumerator->Release();

  if (FAILED(hr)) {
    std::wcerr << L"Fehler beim Abrufen des Standard-Audiogeräts." << std::endl;
    return 6;
  }

  // Endpoint Volume Interface abrufen.
  IAudioEndpointVolume *endpointVolume = NULL;
  hr = defaultDevice->Activate(__uuidof(IAudioEndpointVolume), CLSCTX_INPROC_SERVER, NULL, (LPVOID *)&endpointVolume);
  defaultDevice->Release();

  if (FAILED(hr)) {
    std::wcerr << L"Fehler beim Aktivieren des Audio-Endpunkt-Volumes." << std::endl;
    return 7;
  }

  // If started without parameters, just print current values.

  if (argc == 1) {
    // Query current volume level. Return value is between 0.0 and 1.0.
    float currentVolume = 0.0f;
    hr = endpointVolume->GetMasterVolumeLevelScalar(&currentVolume);
    if (SUCCEEDED(hr)) {
      std::wcout << L"Current volume: " << (currentVolume * 100) << L"%" << std::endl;
    } else {
      std::wcerr << L"Error retrieving the current volume." << std::endl;
      endpointVolume->Release();
      CoUninitialize();
      return 8;
    }

    // Query mute status.
    BOOL isMuted = FALSE;
    hr = endpointVolume->GetMute(&isMuted);
    if (SUCCEEDED(hr)) {
      std::wcout << L"Mute state: " << (isMuted ? L"muted" : L"not muted") << std::endl;
    } else {
      std::wcerr << L"Error retrieving mute state." << std::endl;
      endpointVolume->Release();
      CoUninitialize();
      return 9;
    }
  }

  if (argc == 2) {
    if (mute_set) {                                // Unmute/Mute speakers.
      hr = endpointVolume->SetMute(mute_value, NULL);
      if (SUCCEEDED(hr)) {
        std::wcout << L"Speakers were muted." << std::endl;
      } else {
        std::wcerr << L"Error muting speakers." << std::endl;
        endpointVolume->Release();
        CoUninitialize();
        return 10;
      }
    }

    if (volume_set) {                        // Set speaker volume.
      float newVolume = (float) volume_value / 100.0;
      hr = endpointVolume->SetMasterVolumeLevelScalar(newVolume, NULL);

      if (SUCCEEDED(hr)) {
        std::wcout << L"Volume set to " << volume_value << "%." << std::endl;
      } else {
        std::wcerr << L"Error setting a new volume." << std::endl;
        endpointVolume->Release();
        CoUninitialize();
        return 11;
      }
    }
  }

  endpointVolume->Release();
  CoUninitialize();

  return 0;
}
