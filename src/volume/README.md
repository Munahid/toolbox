# volume

This is a command line application which allows setting the speaker's volume and muting/unmuting them via script.

We use the Windows Core Audio API via COM. Mittels IMMDeviceEnumerator werden die Audioendpunkte des Systems untersucht und es wird auf das Standardwiedergabegerät (typischerweise die Lautsprecher) zugegriffen. Über deren IAudioEndpointVolume-Schnittstelle werden Lautstärke und Stummschaltung des Systems ermittelt bzw. gesetzt.
