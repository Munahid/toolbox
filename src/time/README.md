# time command

Provides a standard time command following the DIN ISO 8601 and DIN EN 28601 rules. It only outputs the current time.
