// This tool returns the current time.

// TODO: Add time zones ("+/-hh:mm").

#ifndef _UNICODE
#define _UNICODE
#endif
#ifndef UNICODE
#define UNICODE
#endif

#define WIN32_LEAN_AND_MEAN		       // Exclude rarely-used stuff from Windows headers.

#include <windows.h>

#include <iostream>

#include "../common/CommandLineArguments.h"

int main(int argc, char *argv[])
{
  CommandLineArguments args;

  args.addParameter("l", "localtime", "Use local time value.", CommandLineArguments::DataType::String, CommandLineArguments::ParaType::Optional);

  args.addInfo(L"Retrieves the current time or local time.");
  args.addHelp(L"No parameters required (then always UTC).");
  args.check();

  //wchar_t localeName[LOCALE_NAME_MAX_LENGTH] = {0}; // 85
  //int rc = GetSystemDefaultLocaleName(localeName, LOCALE_NAME_MAX_LENGTH);

  SYSTEMTIME systemtime;

  //if (args.getArgument("l") == true)
  //  GetLocalTime(&systemtime);
  //else
    GetSystemTime(&systemtime);

  wchar_t buffer[] = L"HH:mm:ss\0";

  // LOCALE_USER_DEFAULT
  if (GetTimeFormatEx(LOCALE_NAME_SYSTEM_DEFAULT, TIME_FORCE24HOURFORMAT,
                      &systemtime, L"HH:mm:ss", buffer, sizeof(buffer)) == 0) {
    std::wcout << "Error." << std::endl;
    return 1;
  }

  std::wcout << buffer << std::endl;

  return 0;
}
