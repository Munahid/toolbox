// File time changer

#include <iostream>
#include <iomanip>
#include <string>
#include <regex>
// #include <codecvt>  // deprecated
#include <locale>

#ifndef UNICODE
#define UNICODE
#endif
#ifndef _UNICODE
#define _UNICODE
#endif

#define WIN32_LEAN_AND_MEAN     // Exclude rarely-used stuff from Windows headers

#include <windows.h>
#include <strsafe.h>
#include <pathcch.h>

/*
using convert_t = std::codecvt_utf8<wchar_t>;
std::wstring_convert<convert_t, wchar_t> strconverter;

std::string to_string(std::wstring wstr)
{
  return strconverter.to_bytes(wstr);
}

std::wstring to_wstring(std::string str)
{
  return strconverter.from_bytes(str);
}
*/

std::wstring stow(std::string& value){
  const size_t cSize = value.size() + 1;

  std::wstring wc;
  wc.resize(cSize);

  size_t cSize1;
  mbstowcs_s(&cSize1, (wchar_t*)&wc[0], cSize, value.c_str(), cSize);

  wc.pop_back();

  return wc;
}

int main(int argc, char *argv[])
{
  if (argc < 2) {
    std::cout << "Too few arguments, argc: " << argc << std::endl;
    return 1;
  }

  std::string s = argv[1];
  std::wstring fileName = stow(s);

  std::cout << "File: " << s << std::endl;
  std::wcout << "File: " << fileName << std::endl;

// scoped_holder<HANDLE, BOOL (__stdcall *)(HANDLE)>
//    h(CreateFile(file.c_str(), GENERIC_WRITE, 0, nullptr, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr), CloseHandle);


  HANDLE fh = CreateFile(fileName.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);

  if (fh == INVALID_HANDLE_VALUE) {
    std::cout << "Error 2 (file not found?)" << std::endl;
    return 2;
  }

  DWORD type = GetFileType(fh);

  if (type != FILE_TYPE_DISK) {
    std::cout << "Error 3 (filetype)" << std::endl;
    CloseHandle(fh);
    return 3;
  }

  FILETIME ftCreate, ftAccess, ftWrite;
  SYSTEMTIME stUTC, stLocal;

  // Retrieve the file times for the file.

  if (!GetFileTime(fh, &ftCreate, &ftAccess, &ftWrite)) {
    std::cout << "Error 10 (getfiletime)" << std::endl;
    CloseHandle(fh);
    return 10;
  }

  // Convert the last-write time to local time.

  FileTimeToSystemTime(&ftWrite, &stUTC);
  SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);

  std::cout << "TIME: "
    << std::setfill('0')
    << std::setw(4) << stLocal.wYear << "-"
    << std::setw(2) << stLocal.wMonth << "-"
    << std::setw(2) << stLocal.wDay
    << " "
    << std::setw(2) << stLocal.wHour << ":"
    << std::setw(2) << stLocal.wMinute << std::endl;

  SYSTEMTIME st;
  FILETIME ft;
  GetSystemTime(&st);
  if(!SystemTimeToFileTime(&st, &ft)) {

    return 8;
  }

  FILETIME /*ftNewCreate, ftNewAccess,*/ ftNewWrite;
  ftNewWrite.dwHighDateTime = 0L;  // date
  ftNewWrite.dwLowDateTime  = 0L;  // time
  if (!SetFileTime(fh, NULL, NULL, &ftNewWrite)) {
    std::cout << "Error 9 (setfiletime)" << std::endl;
    CloseHandle(fh);
    return 9;
  }

  if (CloseHandle(fh) == 0) {
    std::cout << "Error 4 (closehandle)" << std::endl;
    return 4;
  }

  return 0;
}
