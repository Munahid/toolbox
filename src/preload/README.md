# preload command

The Library Preloader project is a little autorun tool which loads libraries (for example the Qt libraries) into memory. This speeds up the start of every application which needs these libraries. The library list is user-configurable. Loading occurs in the background with a low process priority.

The following parameters can be given using a shortcut
to the exe file:

/ABOUT
/ADD
/REMOVE
/REMOVEALL
/QUIT
/START (Default)
