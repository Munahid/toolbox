# wlan-tool

This program provides a list of SSIDs of all available WLANs under Windows and displays information about their security and signal strength. It uses the Windows Wireless LAN API.
