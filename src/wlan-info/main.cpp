//

#ifndef _UNICODE
#define _UNICODE
#endif
#ifndef UNICODE
#define UNICODE
#endif

#define WIN32_LEAN_AND_MEAN		                           // Exclude rarely-used stuff from Windows headers.

#include <windows.h>
#include <wlanapi.h>
#include <objbase.h>
#include <wtypes.h>

#include <iostream>

int main()
{
  HANDLE wlanHandle;
  DWORD  maxClient = 2;      // Maximale Anzahl der Clientversionen.
  DWORD  currentVersion;
  DWORD  rc;

  rc = WlanOpenHandle(maxClient, NULL, &currentVersion, &wlanHandle);
  if (rc != ERROR_SUCCESS) {
    std::wcerr << L"WlanOpenHandle failed, error code: " << rc << std::endl;
    return 1;
  }

  PWLAN_INTERFACE_INFO_LIST interfaceInfoList = NULL;
  rc = WlanEnumInterfaces(wlanHandle, NULL, &interfaceInfoList);
  if (rc != ERROR_SUCCESS) {
    std::wcerr << L"WlanEnumInterfaces failed, error code: " << rc << std::endl;
    WlanCloseHandle(wlanHandle, NULL);
    return 2;
  }

  std::wcout << L"Number of Interfaces: " << interfaceInfoList->dwNumberOfItems << std::endl;

  for (DWORD i = 0; i < interfaceInfoList->dwNumberOfItems; i++) {
    PWLAN_INTERFACE_INFO pIfInfo = &interfaceInfoList->InterfaceInfo[i];

    std::wcout << L"Interface Name: " << pIfInfo->strInterfaceDescription << std::endl;

    PWLAN_AVAILABLE_NETWORK_LIST networkList = NULL;

    rc = WlanGetAvailableNetworkList(wlanHandle, &pIfInfo->InterfaceGuid, 0
         /*WLAN_AVAILABLE_NETWORK_INCLUDE_ALL_ADHOC_PROFILES | WLAN_AVAILABLE_NETWORK_INCLUDE_ALL_MANUAL_HIDDEN_PROFILES*/, NULL, &networkList);
    if (rc != ERROR_SUCCESS) {
      std::wcerr << L"WlanGetAvailableNetworkList failed with error code: " << rc << std::endl;
	  if (rc == 2150899714) // 0x80342002, ERROR_NDIS_DOT11_POWER_STATE_INVALID.
        std::wcout << L"No WLAN." << std::endl;

      continue;
    }

    std::wcout << L"Number of WLANs: " << networkList->dwNumberOfItems << std::endl;

    for (DWORD j = 0; j < networkList->dwNumberOfItems; j++) {
      PWLAN_AVAILABLE_NETWORK network = &networkList->Network[j];

      std::wcout << L"Profile Name: " << network->strProfileName << std::endl; // WCHAR, 0 terminated.
      std::wcout << L"  SSID: ";   // Not 0 terminated!
      if (network->dot11Ssid.uSSIDLength == 0) {
        std::wcout << L"Wildcard."; // The 802.11 station can connect to any basic service set (BSS) network.
      }
      else {
        for (int k = 0; k < (int)network->dot11Ssid.uSSIDLength; k++) {
          std::wcout << (WCHAR) network->dot11Ssid.ucSSID[k];
        }
      }
      std::wcout << std::endl;

      std::wcout << L"  Number of BSSIDs: " << network->uNumberOfBssids << std::endl;

      std::wcout << L"  BSS Network type: ";
      switch (network->dot11BssType) {
        case dot11_BSS_type_infrastructure:
          std::wcout << L"Infrastructure"; break;
        case dot11_BSS_type_independent:
          std::wcout << L"Independent/Ad-hoc"; break;
        default:
          std::wcout << L"Other" << network->dot11BssType; break;
      }
      std::wcout << std::endl;

      std::wcout << L"  Connectable: ";
      if (network->bNetworkConnectable) {
        std::wcout << L"Yes";
      }
      else {
        std::wcout << L"No (reason code: " << network->wlanNotConnectableReason << ")";
      }
      std::wcout << std::endl;

      std::wcout << L"  Security Enabled: ";
      if (network->bSecurityEnabled) {
        std::wcout << L"Yes";
      }
      else {
        std::wcout << L"No";
      }
      std::wcout << std::endl;

      std::wcout << L"  Number of PHY types supported: " << network->uNumberOfPhyTypes << std::endl;

      int rssi = 0;
      if (network->wlanSignalQuality == 0)
        rssi = -100;
      else if (network->wlanSignalQuality == 100)
        rssi = -50;
      else
        rssi = -100 + (network->wlanSignalQuality / 2);
      std::wcout << L"  Signal Quality/Strength: " << network->wlanSignalQuality << L"% (RSSI: " << rssi << L" dBm)" << std::endl;

      std::wcout << L"  Flags: " << (int) network->dwFlags;
      if (network->dwFlags > 0) {
        DWORD connectedFlag    = 0x01; // WLAN_AVAILABLE_NETWORK_CONNECTED
        DWORD hasProfileFlag   = 0x02; // WLAN_AVAILABLE_NETWORK_HAS_PROFILE

        if ((network->dwFlags & connectedFlag) == connectedFlag)
          std::wcout << L" - Currently connected";
	      // Das Netzwerk ist mit diesem Computer verbunden.
        if ((network->dwFlags & hasProfileFlag) == hasProfileFlag)
          std::wcout << L" - Has profile";
          // Das Netzwerk verfügt über ein gespeichertes Profil auf diesem Computer.
      }
      std::wcout << std::endl;

      if (network->dot11DefaultAuthAlgorithm == DOT11_AUTH_ALGO_80211_OPEN &&
          network->dot11DefaultCipherAlgorithm == DOT11_CIPHER_ALGO_NONE) {
        std::wcout << L"  Das Netzwerk hat keine Sicherheitsverschlüsselung (offenes Netzwerk)." << std::endl;
      }
      else {
        std::wcout << L"  Das Netzwerk ist gesichert." << std::endl;
      }
    }

    WlanFreeMemory(networkList);
  }

  WlanFreeMemory(interfaceInfoList);
  WlanCloseHandle(wlanHandle, NULL);

  return 0;
}
