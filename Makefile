# Makefile - builds all tools in the src directory.

all: build test deploy

build:
	 @cd src; ${MAKE} $@

test:


deploy:


clean:
cleanall:
	@echo Cleaning the whole project ...
	cd src; ${MAKE} $@; cd ..
