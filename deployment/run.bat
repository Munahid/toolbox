@echo off

echo Compiling...
wix/candle.exe Toolbox.wxs -out Toolbox.wixobj

echo Linking...
wix/light.exe Toolbox.wixobj -out Toolbox.msi

echo Finished.
